
package com.skall.paysafe.student.database.repository;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.LessonEntity;

public interface LessonRepository extends CrudRepository<LessonEntity, Long> {

}
