
package com.skall.paysafe.student.database.repository;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.CategoryLessonEntity;

public interface CategoryLessonRepository extends CrudRepository<CategoryLessonEntity, Long> {

}
