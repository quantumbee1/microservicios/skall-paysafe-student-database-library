
package com.skall.paysafe.student.database.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(
        name = "user",
        catalog = "education",
        schema = "student")
public class UserEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "user_seq_generator")
    @SequenceGenerator(
            name = "user_seq_generator",
            sequenceName = "student.user_id_seq",
            allocationSize = 1)
    @Basic(
            optional = false)
    @Column(
            name = "id")
    private Long id;
    @Basic(
            optional = false)
    @Column(
            name = "first_name")
    private String firstName;
    @Basic(
            optional = false)
    @Column(
            name = "last_name")
    private String lastName;
    @Column(
            name = "second_last_name")
    private String secondLastName;
    @Column(
            name = "phone_number")
    private String phoneNumber;
    @Basic(
            optional = false)
    @Column(
            name = "email")
    private String email;
    @Basic(
            optional = false)
    @Column(
            name = "password")
    private String password;
    @Basic(
            optional = false)
    @Column(
            name = "document_number")
    private String documentNumber;
    @Column(
            name = "status")
    private Boolean status;
    @Column(
            name = "verify")
    private Boolean verify;
    @Column(
            name = "age")
    private String age;
    @Column(
            name = "job")
    private String job;
    @Column(
            name = "address")
    private String address;
    @Column(
            name = "gender")
    private String gender;
    @Column(
            name = "logo")
    private String logo;

    @Column(
            name = "inserted_ip_address")
    private String insertedIpAddress;
    @Column(
            name = "inserted_username")
    private String insertedUsername;

    @Column(
            name = "inserted_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertedAt;
    @Column(
            name = "updated_ip_address")
    private String updatedIpAddress;
    @Column(
            name = "updated_username")
    private String updatedUsername;
    @Column(
            name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @JoinColumn(
            name = "document_type_id",
            referencedColumnName = "id")
    @ManyToOne(
            optional = false,
            fetch = FetchType.LAZY)
    private DocumentTypeEntity documentTypeId;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "userId",
            fetch = FetchType.LAZY)
    private List<InterestUserEntity> interestUserList;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "userId",
            fetch = FetchType.LAZY)
    private List<RolUserEntity> rolUserList;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "userId",
            fetch = FetchType.LAZY)
    private List<SkillUserEntity> skillList;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "userId",
            fetch = FetchType.LAZY)
    private List<AcademicTrainingUserEntity> academicTrainingUserList;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "userId",
            fetch = FetchType.LAZY)
    private List<SubscriptionUserEntity> subscriptionUserList;

    @JoinColumn(
            name = "parlor_id",
            referencedColumnName = "id")
    @ManyToOne(
            optional = false,
            fetch = FetchType.LAZY)
    private ParlorEntity parlorId;

    public List<InterestUserEntity> getInterestUserList() {
        return interestUserList;
    }

    public void setInterestUserList(List<InterestUserEntity> interestUserList) {
        this.interestUserList = interestUserList;
    }

    public List<RolUserEntity> getRolUserList() {
        return rolUserList;
    }

    public void setRolUserList(List<RolUserEntity> rolUserList) {
        this.rolUserList = rolUserList;
    }

    public List<SkillUserEntity> getSkillList() {
        return skillList;
    }

    public void setSkillList(List<SkillUserEntity> skillList) {
        this.skillList = skillList;
    }

    public List<AcademicTrainingUserEntity> getAcademicTrainingUserList() {
        return academicTrainingUserList;
    }

    public void
            setAcademicTrainingUserList(List<AcademicTrainingUserEntity> academicTrainingUserList) {
        this.academicTrainingUserList = academicTrainingUserList;
    }

    public List<SubscriptionUserEntity> getSubscriptionUserList() {
        return subscriptionUserList;
    }

    public void setSubscriptionUserList(List<SubscriptionUserEntity> subscriptionUserList) {
        this.subscriptionUserList = subscriptionUserList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getVerify() {
        return verify;
    }

    public void setVerify(Boolean verify) {
        this.verify = verify;
    }

    public String getInsertedIpAddress() {
        return insertedIpAddress;
    }

    public void setInsertedIpAddress(String insertedIpAddress) {
        this.insertedIpAddress = insertedIpAddress;
    }

    public String getInsertedUsername() {
        return insertedUsername;
    }

    public void setInsertedUsername(String insertedUsername) {
        this.insertedUsername = insertedUsername;
    }

    public Date getInsertedAt() {
        return insertedAt;
    }

    public void setInsertedAt(Date insertedAt) {
        this.insertedAt = insertedAt;
    }

    public String getUpdatedIpAddress() {
        return updatedIpAddress;
    }

    public void setUpdatedIpAddress(String updatedIpAddress) {
        this.updatedIpAddress = updatedIpAddress;
    }

    public String getUpdatedUsername() {
        return updatedUsername;
    }

    public void setUpdatedUsername(String updatedUsername) {
        this.updatedUsername = updatedUsername;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public DocumentTypeEntity getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(DocumentTypeEntity documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public ParlorEntity getParlorId() {
        return parlorId;
    }

    public void setParlorId(ParlorEntity parlorId) {
        this.parlorId = parlorId;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserEntity other = (UserEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @PrePersist
    public void prePersist() {
        this.insertedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = new Date();
    }

    @Override
    public String toString() {
        return "UserEntity [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName
                + ", secondLastName=" + secondLastName + ", phoneNumber=" + phoneNumber + ", email="
                + email + ", password=" + password + ", documentNumber=" + documentNumber
                + ", status=" + status + ", verify=" + verify + ", age=" + age + ", job=" + job
                + ", address=" + address + ", gender=" + gender + ", logo=" + logo
                + ", insertedIpAddress=" + insertedIpAddress + ", insertedUsername="
                + insertedUsername + ", insertedAt=" + insertedAt + ", updatedIpAddress="
                + updatedIpAddress + ", updatedUsername=" + updatedUsername + ", updatedAt="
                + updatedAt + "]";
    }

}
