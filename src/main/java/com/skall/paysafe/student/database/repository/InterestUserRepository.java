
package com.skall.paysafe.student.database.repository;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.InterestUserEntity;

public interface InterestUserRepository extends CrudRepository<InterestUserEntity, Long> {

}
