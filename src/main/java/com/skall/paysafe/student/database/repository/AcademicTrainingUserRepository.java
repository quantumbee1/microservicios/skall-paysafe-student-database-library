
package com.skall.paysafe.student.database.repository;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.AcademicTrainingEntity;
import com.skall.paysafe.student.database.entity.AcademicTrainingUserEntity;
import com.skall.paysafe.student.database.entity.UserEntity;

public interface AcademicTrainingUserRepository
        extends CrudRepository<AcademicTrainingUserEntity, Long> {
    public AcademicTrainingUserEntity
            findByUserIdAndAcademicTrainingIdAndLevelOfStudyAndStatusIsTrue(UserEntity UserId,
                    AcademicTrainingEntity AcademicTrainingId, String LevelOfStudy);
}
