
package com.skall.paysafe.student.database.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skall.paysafe.student.database.entity.DocumentTypeEntity;

@Repository
public interface DocumentTypeRepository extends CrudRepository<DocumentTypeEntity, Long> {
    public Optional<DocumentTypeEntity> findByName(final String name);
}
