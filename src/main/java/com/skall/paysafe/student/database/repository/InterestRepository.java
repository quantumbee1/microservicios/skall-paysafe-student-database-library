
package com.skall.paysafe.student.database.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.InterestEntity;

public interface InterestRepository extends CrudRepository<InterestEntity, Long> {
    public Optional<InterestEntity> findByName(final String name);
}
