
package com.skall.paysafe.student.database.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(
        name = "country",
        catalog = "education",
        schema = "student")
public class CountryEntity implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "country_seq_generator")
    @SequenceGenerator(
            name = "country_seq_generator",
            sequenceName = "student.country_id_seq",
            allocationSize = 1)
    @Basic(
            optional = false)
    @Column(
            name = "id")
    private Long id;
    @Basic(
            optional = false)
    @Column(
            name = "name")
    private String name;
    @Basic(
            optional = false)
    @Column(
            name = "iso_alpha_2")
    private String isoAlpha2;
    @Column(
            name = "status")
    private Boolean status;
    @Basic(
            optional = false)
    @Column(
            name = "phone_code")
    private String phoneCode;
    @Basic(
            optional = false)
    @Column(
            name = "zone_time")
    private String zoneTime;
    @Column(
            name = "inserted_ip_address")
    private String insertedIpAddress;
    @Column(
            name = "inserted_username")
    private String insertedUsername;
    @Column(
            name = "inserted_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertedAt;
    @Column(
            name = "updated_ip_address")
    private String updatedIpAddress;
    @Column(
            name = "updated_username")
    private String updatedUsername;
    @Column(
            name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "countryId",
            fetch = FetchType.LAZY)
    private List<DocumentTypeEntity> documentTypeList;

    @PrePersist
    public void prePersist() {
        this.status = true;
        this.insertedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = new Date();
    }

    public List<DocumentTypeEntity> getDocumentTypeList() {
        return documentTypeList;
    }

    public void setDocumentTypeList(List<DocumentTypeEntity> documentTypeList) {
        this.documentTypeList = documentTypeList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsoAlpha2() {
        return isoAlpha2;
    }

    public void setIsoAlpha2(String isoAlpha2) {
        this.isoAlpha2 = isoAlpha2;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getZoneTime() {
        return zoneTime;
    }

    public void setZoneTime(String zoneTime) {
        this.zoneTime = zoneTime;
    }

    public String getInsertedIpAddress() {
        return insertedIpAddress;
    }

    public void setInsertedIpAddress(String insertedIpAddress) {
        this.insertedIpAddress = insertedIpAddress;
    }

    public String getInsertedUsername() {
        return insertedUsername;
    }

    public void setInsertedUsername(String insertedUsername) {
        this.insertedUsername = insertedUsername;
    }

    public Date getInsertedAt() {
        return insertedAt;
    }

    public void setInsertedAt(Date insertedAt) {
        this.insertedAt = insertedAt;
    }

    public String getUpdatedIpAddress() {
        return updatedIpAddress;
    }

    public void setUpdatedIpAddress(String updatedIpAddress) {
        this.updatedIpAddress = updatedIpAddress;
    }

    public String getUpdatedUsername() {
        return updatedUsername;
    }

    public void setUpdatedUsername(String updatedUsername) {
        this.updatedUsername = updatedUsername;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CountryEntity other = (CountryEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "CountryEntity [id=" + id + ", name=" + name + ", isoAlpha2=" + isoAlpha2
                + ", status=" + status + ", phoneCode=" + phoneCode + ", zoneTime=" + zoneTime
                + ", insertedIpAddress=" + insertedIpAddress + ", insertedUsername="
                + insertedUsername + ", insertedAt=" + insertedAt + ", updatedIpAddress="
                + updatedIpAddress + ", updatedUsername=" + updatedUsername + ", updatedAt="
                + updatedAt + "]";
    }

}
