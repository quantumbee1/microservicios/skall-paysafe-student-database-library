
package com.skall.paysafe.student.database.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(
        name = "academic_training",
        catalog = "education",
        schema = "student")
public class AcademicTrainingEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5274232111005141954L;
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "academic_training_seq_generator")
    @SequenceGenerator(
            name = "academic_training_seq_generator",
            sequenceName = "student.academic_training_id_seq",
            allocationSize = 1)
    @Basic(
            optional = false)
    @Column(
            name = "id")
    private Long id;
    @Basic(
            optional = false)
    @Column(
            name = "name")
    private String name;
    @Column(
            name = "status")
    private Boolean status;
    @Basic(
            optional = false)
    @Column(
            name = "specialty")
    private String specialty;
    @Column(
            name = "inserted_ip_address")
    private String insertedIpAddress;
    @Column(
            name = "inserted_username")
    private String insertedUsername;

    @Column(
            name = "inserted_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertedAt;
    @Column(
            name = "updated_ip_address")
    private String updatedIpAddress;
    @Column(
            name = "updated_username")
    private String updatedUsername;
    @Column(
            name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "academicTrainingId",
            fetch = FetchType.LAZY)
    private List<AcademicTrainingUserEntity> academicTrainingUserList;

    @PrePersist
    public void prePersist() {
        this.insertedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getInsertedIpAddress() {
        return insertedIpAddress;
    }

    public void setInsertedIpAddress(String insertedIpAddress) {
        this.insertedIpAddress = insertedIpAddress;
    }

    public String getInsertedUsername() {
        return insertedUsername;
    }

    public void setInsertedUsername(String insertedUsername) {
        this.insertedUsername = insertedUsername;
    }

    public Date getInsertedAt() {
        return insertedAt;
    }

    public void setInsertedAt(Date insertedAt) {
        this.insertedAt = insertedAt;
    }

    public String getUpdatedIpAddress() {
        return updatedIpAddress;
    }

    public void setUpdatedIpAddress(String updatedIpAddress) {
        this.updatedIpAddress = updatedIpAddress;
    }

    public String getUpdatedUsername() {
        return updatedUsername;
    }

    public void setUpdatedUsername(String updatedUsername) {
        this.updatedUsername = updatedUsername;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<AcademicTrainingUserEntity> getAcademicTrainingUserList() {
        return academicTrainingUserList;
    }

    public void
            setAcademicTrainingUserList(List<AcademicTrainingUserEntity> academicTrainingUserList) {
        this.academicTrainingUserList = academicTrainingUserList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AcademicTrainingEntity other = (AcademicTrainingEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "AcademicTrainingEntity [id=" + id + ", name=" + name + ", status=" + status
                + ", specialty=" + specialty + ", insertedIpAddress=" + insertedIpAddress
                + ", insertedUsername=" + insertedUsername + ", insertedAt=" + insertedAt
                + ", updatedIpAddress=" + updatedIpAddress + ", updatedUsername=" + updatedUsername
                + ", updatedAt=" + updatedAt + "]";
    }

}
