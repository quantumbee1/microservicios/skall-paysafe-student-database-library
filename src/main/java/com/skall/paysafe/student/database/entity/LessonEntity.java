
package com.skall.paysafe.student.database.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(
        name = "lesson",
        catalog = "education",
        schema = "student")
public class LessonEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1019322782377890078L;
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "lesson_seq_generator")
    @SequenceGenerator(
            name = "lesson_seq_generator",
            sequenceName = "student.lesson_id_seq",
            allocationSize = 1)
    @Basic(
            optional = false)
    @Column(
            name = "id")
    private Long id;
    @Basic(
            optional = false)
    @Column(
            name = "name")
    private String name;
    @Column(
            name = "description")
    private String description;
    @Basic(
            optional = false)
    @Column(
            name = "code")
    private String code;
    @Column(
            name = "password")
    private String password;
    @Column(
            name = "status")
    private Boolean status;

    @JoinColumn(
            name = "parlor_id",
            referencedColumnName = "id")
    @ManyToOne(
            optional = false,
            fetch = FetchType.LAZY)
    private ParlorEntity parlorId;

    @JoinColumn(
            name = "category_lesson_id",
            referencedColumnName = "id")
    @ManyToOne(
            optional = false,
            fetch = FetchType.LAZY)
    private CategoryLessonEntity categoryLessonId;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "lessonId",
            fetch = FetchType.LAZY)
    private List<SubscriptionUserEntity> subscriptionUserList;

    @Column(
            name = "inserted_ip_address")
    private String insertedIpAddress;
    @Column(
            name = "inserted_username")
    private String insertedUsername;
    @Column(
            name = "inserted_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertedAt;
    @Column(
            name = "updated_ip_address")
    private String updatedIpAddress;
    @Column(
            name = "updated_username")
    private String updatedUsername;
    @Column(
            name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public List<SubscriptionUserEntity> getSubscriptionUserList() {
        return subscriptionUserList;
    }

    public void setSubscriptionUserList(List<SubscriptionUserEntity> subscriptionUserList) {
        this.subscriptionUserList = subscriptionUserList;
    }

    @PrePersist
    public void prePersist() {
        this.insertedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ParlorEntity getParlorId() {
        return parlorId;
    }

    public void setParlorId(ParlorEntity parlorId) {
        this.parlorId = parlorId;
    }

    public CategoryLessonEntity getCategoryLessonId() {
        return categoryLessonId;
    }

    public void setCategoryLessonId(CategoryLessonEntity categoryLessonId) {
        this.categoryLessonId = categoryLessonId;
    }

    public String getInsertedIpAddress() {
        return insertedIpAddress;
    }

    public void setInsertedIpAddress(String insertedIpAddress) {
        this.insertedIpAddress = insertedIpAddress;
    }

    public String getInsertedUsername() {
        return insertedUsername;
    }

    public void setInsertedUsername(String insertedUsername) {
        this.insertedUsername = insertedUsername;
    }

    public Date getInsertedAt() {
        return insertedAt;
    }

    public void setInsertedAt(Date insertedAt) {
        this.insertedAt = insertedAt;
    }

    public String getUpdatedIpAddress() {
        return updatedIpAddress;
    }

    public void setUpdatedIpAddress(String updatedIpAddress) {
        this.updatedIpAddress = updatedIpAddress;
    }

    public String getUpdatedUsername() {
        return updatedUsername;
    }

    public void setUpdatedUsername(String updatedUsername) {
        this.updatedUsername = updatedUsername;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LessonEntity other = (LessonEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "LessonEntity [id=" + id + ", name=" + name + ", description=" + description
                + ", code=" + code + ", password=" + password + ", status=" + status
                + ", insertedIpAddress=" + insertedIpAddress + ", insertedUsername="
                + insertedUsername + ", insertedAt=" + insertedAt + ", updatedIpAddress="
                + updatedIpAddress + ", updatedUsername=" + updatedUsername + ", updatedAt="
                + updatedAt + "]";
    }

}
