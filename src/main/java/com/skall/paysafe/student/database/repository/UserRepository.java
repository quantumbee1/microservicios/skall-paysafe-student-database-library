
package com.skall.paysafe.student.database.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
    public Optional<UserEntity> findByEmailAndStatusIsTrue(final String email);

    public Optional<UserEntity> findByEmailAndStatusIsTrueAndVerifyIsFalse(final String email);
}
