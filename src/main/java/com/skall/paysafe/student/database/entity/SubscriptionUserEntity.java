
package com.skall.paysafe.student.database.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(
        name = "subscription_user",
        catalog = "education",
        schema = "student")
public class SubscriptionUserEntity implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -7101224805299631636L;
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "subscription_user_seq_generator")
    @SequenceGenerator(
            name = "subscription_user_seq_generator",
            sequenceName = "student.subscription_user_id_seq",
            allocationSize = 1)
    @Basic(
            optional = false)
    @Column(
            name = "id")
    private Long id;
    @Column(
            name = "status")
    private Boolean status;
    @Column(
            name = "inserted_ip_address")
    private String insertedIpAddress;
    @Column(
            name = "inserted_username")
    private String insertedUsername;
    @Column(
            name = "inserted_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertedAt;
    @Column(
            name = "updated_ip_address")
    private String updatedIpAddress;
    @Column(
            name = "updated_username")
    private String updatedUsername;
    @Column(
            name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @JoinColumn(
            name = "user_id",
            referencedColumnName = "id")
    @ManyToOne(
            optional = false,
            fetch = FetchType.LAZY)
    private UserEntity userId;

    @JoinColumn(
            name = "lesson_id",
            referencedColumnName = "id")
    @ManyToOne(
            optional = false,
            fetch = FetchType.LAZY)
    private LessonEntity lessonId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getInsertedIpAddress() {
        return insertedIpAddress;
    }

    public void setInsertedIpAddress(String insertedIpAddress) {
        this.insertedIpAddress = insertedIpAddress;
    }

    public String getInsertedUsername() {
        return insertedUsername;
    }

    public void setInsertedUsername(String insertedUsername) {
        this.insertedUsername = insertedUsername;
    }

    public Date getInsertedAt() {
        return insertedAt;
    }

    public void setInsertedAt(Date insertedAt) {
        this.insertedAt = insertedAt;
    }

    public String getUpdatedIpAddress() {
        return updatedIpAddress;
    }

    public void setUpdatedIpAddress(String updatedIpAddress) {
        this.updatedIpAddress = updatedIpAddress;
    }

    public String getUpdatedUsername() {
        return updatedUsername;
    }

    public void setUpdatedUsername(String updatedUsername) {
        this.updatedUsername = updatedUsername;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public UserEntity getUserId() {
        return userId;
    }

    public void setUserId(UserEntity userId) {
        this.userId = userId;
    }

    public LessonEntity getLessonId() {
        return lessonId;
    }

    public void setLessonId(LessonEntity lessonId) {
        this.lessonId = lessonId;
    }

    @PrePersist
    public void prePersist() {
        this.insertedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = new Date();
    }

    @Override
    public String toString() {
        return "SubscriptionUserEntity [id=" + id + ", status=" + status + ", insertedIpAddress="
                + insertedIpAddress + ", insertedUsername=" + insertedUsername + ", insertedAt="
                + insertedAt + ", updatedIpAddress=" + updatedIpAddress + ", updatedUsername="
                + updatedUsername + ", updatedAt=" + updatedAt + "]";
    }

}
