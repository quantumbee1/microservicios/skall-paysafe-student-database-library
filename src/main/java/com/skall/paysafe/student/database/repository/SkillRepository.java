
package com.skall.paysafe.student.database.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.SkillEntity;

public interface SkillRepository extends CrudRepository<SkillEntity, Long> {

    public Optional<SkillEntity> findByName(String name);
}
