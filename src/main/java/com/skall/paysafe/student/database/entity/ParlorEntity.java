
package com.skall.paysafe.student.database.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(
        name = "parlor",
        catalog = "education",
        schema = "student")
public class ParlorEntity implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "parlor_seq_generator")
    @SequenceGenerator(
            name = "parlor_seq_generator",
            sequenceName = "student.parlor_id_seq",
            allocationSize = 1)
    @Basic(
            optional = false)
    @Column(
            name = "id")
    private Long id;
    @Basic(
            optional = false)
    @Column(
            name = "name")
    private String name;
    @Basic(
            optional = false)
    @Column(
            name = "email")
    private String email;
    @Basic(
            optional = false)
    @Column(
            name = "document_number")
    private String documentNumber;
    @Column(
            name = "phone_number")
    private String phoneNumber;
    @Column(
            name = "parlor_description")
    private String parlorDescription;
    @Column(
            name = "logo")
    private String logo;
    @Column(
            name = "notify_url")
    private String notifyUrl;
    @Basic(
            optional = false)
    @Column(
            name = "api_key")
    private String apiKey;
    @Basic(
            optional = false)
    @Column(
            name = "api_login")
    private String apiLogin;
    @Basic(
            optional = false)
    @Column(
            name = "public_key")
    private String publicKey;
    @Column(
            name = "status")
    private Boolean status;
    @Basic(
            optional = false)
    @Column(
            name = "subscribers")
    private Integer subscribers;
    @Column(
            name = "inserted_ip_address")
    private String insertedIpAddress;
    @Column(
            name = "inserted_username")
    private String insertedUsername;
    @Column(
            name = "inserted_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertedAt;
    @Column(
            name = "updated_ip_address")
    private String updatedIpAddress;
    @Column(
            name = "updated_username")
    private String updatedUsername;
    @Column(
            name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "parlorId",
            fetch = FetchType.LAZY)
    private List<UserEntity> usertList;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "parlorId",
            fetch = FetchType.LAZY)
    private List<LessonEntity> lessontList;

    @JoinColumn(
            name = "document_type_id",
            referencedColumnName = "id")
    @ManyToOne(
            optional = false,
            fetch = FetchType.LAZY)
    private DocumentTypeEntity documentTypeId;

    @PrePersist
    public void prePersist() {
        this.insertedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = new Date();
    }

    public List<LessonEntity> getLessontList() {
        return lessontList;
    }

    public void setLessontList(List<LessonEntity> lessontList) {
        this.lessontList = lessontList;
    }

    public ParlorEntity() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getParlorDescription() {
        return parlorDescription;
    }

    public void setParlorDescription(String parlorDescription) {
        this.parlorDescription = parlorDescription;
    }

    public Integer getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(Integer subscribers) {
        this.subscribers = subscribers;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiLogin() {
        return apiLogin;
    }

    public void setApiLogin(String apiLogin) {
        this.apiLogin = apiLogin;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getInsertedIpAddress() {
        return insertedIpAddress;
    }

    public void setInsertedIpAddress(String insertedIpAddress) {
        this.insertedIpAddress = insertedIpAddress;
    }

    public String getInsertedUsername() {
        return insertedUsername;
    }

    public void setInsertedUsername(String insertedUsername) {
        this.insertedUsername = insertedUsername;
    }

    public Date getInsertedAt() {
        return insertedAt;
    }

    public void setInsertedAt(Date insertedAt) {
        this.insertedAt = insertedAt;
    }

    public String getUpdatedIpAddress() {
        return updatedIpAddress;
    }

    public void setUpdatedIpAddress(String updatedIpAddress) {
        this.updatedIpAddress = updatedIpAddress;
    }

    public String getUpdatedUsername() {
        return updatedUsername;
    }

    public void setUpdatedUsername(String updatedUsername) {
        this.updatedUsername = updatedUsername;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<UserEntity> getUsertList() {
        return usertList;
    }

    public void setUsertList(List<UserEntity> usertList) {
        this.usertList = usertList;
    }

    public DocumentTypeEntity getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(DocumentTypeEntity documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ParlorEntity other = (ParlorEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public ParlorEntity(Long id) {
        this.id = id;
    }

    public ParlorEntity(String name, String email) {
        this.name = name;
        this.email = email;
    }

    @Override
    public String toString() {
        return "ParlorEntity [id=" + id + ", name=" + name + ", email=" + email
                + ", documentNumber=" + documentNumber + ", phoneNumber=" + phoneNumber
                + ", parlorDescription=" + parlorDescription + ", logo=" + logo + ", notifyUrl="
                + notifyUrl + ", apiKey=" + apiKey + ", apiLogin=" + apiLogin + ", publicKey="
                + publicKey + ", status=" + status + ", subscribers=" + subscribers
                + ", insertedIpAddress=" + insertedIpAddress + ", insertedUsername="
                + insertedUsername + ", insertedAt=" + insertedAt + ", updatedIpAddress="
                + updatedIpAddress + ", updatedUsername=" + updatedUsername + ", updatedAt="
                + updatedAt + "]";
    }

}
