
package com.skall.paysafe.student.database.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skall.paysafe.student.database.entity.CountryEntity;

@Repository
public interface CountryRepository extends CrudRepository<CountryEntity, Long> {

}
