
package com.skall.paysafe.student.database.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.RolUserEntity;

public interface RolUserRepository extends CrudRepository<RolUserEntity, Long> {
    @Override
    public List<RolUserEntity> findAll();

}
