
package com.skall.paysafe.student.database.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.skall.paysafe.student.database.entity.RolEntity;

@Repository
public interface RolRepository extends CrudRepository<RolEntity, Long> {
    @Override
    public List<RolEntity> findAll();

    public RolEntity findByName(String name);

    @Query("SELECT rl\n" + "FROM RolUserEntity ru\n" + "INNER JOIN ru.userId ur\n"
            + "INNER JOIN ru.rolId rl\n" + "WHERE ur.email = :email")
    public List<RolEntity> findByRoleUserAndByEmail(@Param("email") String email);
}
