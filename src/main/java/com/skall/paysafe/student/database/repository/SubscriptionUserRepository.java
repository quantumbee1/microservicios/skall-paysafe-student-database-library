
package com.skall.paysafe.student.database.repository;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.SubscriptionUserEntity;

public interface SubscriptionUserRepository extends CrudRepository<SubscriptionUserEntity, Long> {

}
