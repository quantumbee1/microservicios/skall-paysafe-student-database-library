
package com.skall.paysafe.student.database.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skall.paysafe.student.database.entity.ParlorEntity;

@Repository
public interface ParlorRepository extends CrudRepository<ParlorEntity, Long> {
    @Override
    public List<ParlorEntity> findAll();

    public ParlorEntity findById(ParlorEntity parlorId);
}
