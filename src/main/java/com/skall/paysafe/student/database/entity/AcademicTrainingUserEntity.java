
package com.skall.paysafe.student.database.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(
        name = "academic_training_user",
        catalog = "education",
        schema = "student")
public class AcademicTrainingUserEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7731346369428262093L;
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "academic_training_user_seq_generator")
    @SequenceGenerator(
            name = "academic_training_user_seq_generator",
            sequenceName = "student.academic_training_user_id_seq",
            allocationSize = 1)
    @Basic(
            optional = false)
    @Column(
            name = "id")
    private Long id;
    @Column(
            name = "status")
    private Boolean status;
    @Column(
            name = "level_of_study")
    private String levelOfStudy;
    @Column(
            name = "start_training")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTraining;
    @Column(
            name = "end_training")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTraining;
    @Column(
            name = "inserted_ip_address")
    private String insertedIpAddress;
    @Column(
            name = "inserted_username")
    private String insertedUsername;
    @Column(
            name = "inserted_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertedAt;
    @Column(
            name = "updated_ip_address")
    private String updatedIpAddress;
    @Column(
            name = "updated_username")
    private String updatedUsername;
    @Column(
            name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @JoinColumn(
            name = "user_id",
            referencedColumnName = "id")
    @ManyToOne(
            optional = false,
            fetch = FetchType.LAZY)
    private UserEntity userId;

    @JoinColumn(
            name = "academic_training_id",
            referencedColumnName = "id")
    @ManyToOne(
            optional = false,
            fetch = FetchType.LAZY)
    private AcademicTrainingEntity academicTrainingId;

    @PrePersist
    public void prePersist() {
        this.insertedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getLevelOfStudy() {
        return levelOfStudy;
    }

    public void setLevelOfStudy(String levelOfStudy) {
        this.levelOfStudy = levelOfStudy;
    }

    public Date getStartTraining() {
        return startTraining;
    }

    public void setStartTraining(Date startTraining) {
        this.startTraining = startTraining;
    }

    public Date getEndTraining() {
        return endTraining;
    }

    public void setEndTraining(Date endTraining) {
        this.endTraining = endTraining;
    }

    public String getInsertedIpAddress() {
        return insertedIpAddress;
    }

    public void setInsertedIpAddress(String insertedIpAddress) {
        this.insertedIpAddress = insertedIpAddress;
    }

    public String getInsertedUsername() {
        return insertedUsername;
    }

    public void setInsertedUsername(String insertedUsername) {
        this.insertedUsername = insertedUsername;
    }

    public Date getInsertedAt() {
        return insertedAt;
    }

    public void setInsertedAt(Date insertedAt) {
        this.insertedAt = insertedAt;
    }

    public String getUpdatedIpAddress() {
        return updatedIpAddress;
    }

    public void setUpdatedIpAddress(String updatedIpAddress) {
        this.updatedIpAddress = updatedIpAddress;
    }

    public String getUpdatedUsername() {
        return updatedUsername;
    }

    public void setUpdatedUsername(String updatedUsername) {
        this.updatedUsername = updatedUsername;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public UserEntity getUserId() {
        return userId;
    }

    public void setUserId(UserEntity userId) {
        this.userId = userId;
    }

    public AcademicTrainingEntity getAcademicTrainingId() {
        return academicTrainingId;
    }

    public void setAcademicTrainingId(AcademicTrainingEntity academicTrainingId) {
        this.academicTrainingId = academicTrainingId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AcademicTrainingUserEntity other = (AcademicTrainingUserEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "AcademicTrainingUserEntity [id=" + id + ", status=" + status + ", levelOfStudy="
                + levelOfStudy + ", startTraining=" + startTraining + ", endTraining=" + endTraining
                + ", insertedIpAddress=" + insertedIpAddress + ", insertedUsername="
                + insertedUsername + ", insertedAt=" + insertedAt + ", updatedIpAddress="
                + updatedIpAddress + ", updatedUsername=" + updatedUsername + ", updatedAt="
                + updatedAt + "]";
    }

}
