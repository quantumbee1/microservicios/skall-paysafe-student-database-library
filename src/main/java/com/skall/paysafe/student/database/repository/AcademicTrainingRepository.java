
package com.skall.paysafe.student.database.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.AcademicTrainingEntity;

public interface AcademicTrainingRepository extends CrudRepository<AcademicTrainingEntity, Long> {

    public Optional<AcademicTrainingEntity> findByNameAndSpecialty(final String name,
            final String specialty);
}
