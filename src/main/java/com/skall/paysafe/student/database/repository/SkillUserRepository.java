
package com.skall.paysafe.student.database.repository;

import org.springframework.data.repository.CrudRepository;

import com.skall.paysafe.student.database.entity.SkillEntity;
import com.skall.paysafe.student.database.entity.SkillUserEntity;
import com.skall.paysafe.student.database.entity.UserEntity;

public interface SkillUserRepository extends CrudRepository<SkillUserEntity, Long> {

    public Boolean findByUserIdAndSkillId(UserEntity userId, SkillEntity skillId);
}
